# Terraform Customer Analaytics

Terraform provisioning for creating isolated analytic environments providing:

- **Dedicated resources**
  - Avoids multitenancy risks
- **Modularity**
  - Extending to new customers is easy
  - Upgrading existing modules benefit all customers
  - Customers can "subscribe" to resource needs/availability that fit their needs
- **Isolated IAM**
  - IAM resources are managed independently of internal users
  - IAM can be made customer-managed


## Setup

These instructions pertain to initial setup (or for CI/CD):

1. Download/install Azure CLI
2. Login to Azure: `az login`
3. Create Terraform State resources: `$ ./az-remote-state`
4. Make note of script return and:
   - Modify `config.tf` and `remote_states.tf` for applicable environments (e.g. `./env/dev`)
   - Setup environment variable: `export ARM_ACCESS_KEY="<your token>"`

### Extra: Alternative Remote State Auth methods

The below alternative means exist for authenticating against the remote state:

- `ARM_ACCESS_KEY`
- `ARM_SAS_TOKEN`
- `ARM_CLIENT_ID`
- `ARM_USE_MSI`, `ARM_SUBSCRIPTION_ID` & `ARM_TENANT_ID`


## TF variables

Secrets are stored as TF variables and need to be passed. These can be passed as one of the following methods:

- Environment variables, added as: `TF_VAR_var`, e.g. `TF_VAR_db_pswd_customer`
- Leveraging a `.tfvars` file and pass as `-var-file=terraform.tfvars`


## Disclaimer

**THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.**