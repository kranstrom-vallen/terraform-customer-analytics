output "resource_group_name" {
  value       = azurerm_resource_group.main.name
  description = "Operations Resource Group"
}

output "key_vault_id" {
  value       = azurerm_key_vault.main.id
  description = "Operations Key Vault Id"
}

output "key_vault_name" {
  value       = azurerm_key_vault.main.name
  description = "Operations Key Vault Name"
}