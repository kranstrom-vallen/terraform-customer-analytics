data "terraform_remote_state" "main" {
  backend = "azurerm"
  config = {
    storage_account_name = "tstate31368"
    container_name       = "tstate"
    key                  = "dev.terraform.tfstate"
  }
}

data "terraform_remote_state" "ops" {
  backend = "azurerm"
  config = {
    storage_account_name = "tstate31368"
    container_name       = "tstate"
    key                  = "ops.terraform.tfstate"
  }
}