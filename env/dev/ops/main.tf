# Cust001 Dev
module "cust001" {
  source = "../../modules/stack-analytics"

  customer     = "c001"
  environment  = "dev"
  admin_emails = ["${var.admin_email}"]
  db_pswd      = var.db_pswd_cust001
  location     = "East US 2"
}
