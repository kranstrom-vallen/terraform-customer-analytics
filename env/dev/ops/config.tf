terraform {
  backend "azurerm" {
    resource_group_name  = "tstate"
    storage_account_name = "tstate31368"
    container_name       = "tstate"
    key                  = "dev.terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
  version = "~> 2.0"
}