# Ops
data "azurerm_key_vault" "ops" {
  name                = data.terraform_remote_state.ops.outputs.key_vault_name
  resource_group_name = data.terraform_remote_state.ops.outputs.resource_group_name
}

data "azurerm_key_vault_secret" "admin_email" {
  name         = "admin-email"
  key_vault_id = data.azurerm_key_vault.ops.id
}

# Cust001 Dev
data "azurerm_key_vault_secret" "cust001" {
  name         = "c001-db-dev-pswd"
  key_vault_id = data.azurerm_key_vault.ops.id
}

module "cust001" {
  source = "../../modules/stack-analytics"

  customer     = "c001"
  environment  = "dev"
  admin_emails = ["${data.azurerm_key_vault_secret.admin_email.value}"]
  db_pswd      = data.azurerm_key_vault_secret.cust001.value
  location     = "East US 2"
}
