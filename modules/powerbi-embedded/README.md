# Module: Power BI Embedded

This module isn't yet supported by the Terraform Azure provider. For now, deploy as:

```
$ az group deployment create -g <resource-group> \
  --template-uri https://bitbucket.org/kranstrom-vallen/terraform-customer-analytics/src/master/modules/powerbi-embedded/template-src.json \
  --parameters "{\"customer\": {\"value\": \"<customer>\"}, \"environment\": {\"value\": \"dev\"}}" \
  --debug
```
