data "template_file" "powerbi" {
  template = "${templatefile("${path.module}/template.json",
    {
      name     = "pbicid${var.customer}",
      location = var.location,
      sku_name = var.sku_name,
      sku_tier = var.sku_tier
      tags = {
        Organization = var.customer
        Resource     = "analytics"
        Environment  = var.environment
      }
  })}"
}