resource "azurerm_postgresql_server" "default" {
  name                = "dw-pg-${var.customer}"
  location            = var.location
  resource_group_name = var.resource_group_name

  sku_name = var.sku_name

  administrator_login          = var.db_user
  administrator_login_password = var.db_pswd
  version                      = var.db_version
  ssl_enforcement              = "Enabled"

  storage_profile {
    storage_mb            = var.db_server_storage_mb
    backup_retention_days = var.db_server_backup_retention_days
    geo_redundant_backup  = var.db_server_geo_redundant_backup
    auto_grow             = var.db_server_auto_grow
  }

  tags = {
    Organization = var.customer
    Resource     = "storage"
    Environment  = var.environment
  }
}

resource "azurerm_postgresql_database" "default" {
  name                = "dw-pg-db-${var.customer}"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.default.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}

resource "azurerm_postgresql_firewall_rule" "default" {
  name                = "dw-pg-fwr-${var.customer}"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.default.name
  start_ip_address    = var.start_ip_address
  end_ip_address      = var.end_ip_address
}

# resource "azurerm_postgresql_configuration" "default" {
#   name                = "backslash_quote"
#   resource_group_name = var.resource_group_name
#   server_name         = azurerm_postgresql_server.default.name
#   value               = "on"
# }