# Customer/Environment
variable "customer" {}
variable "environment" {}

# Shared
variable "resource_group_name" {}
variable "location" {}

# DB-server specific
variable "db_user" {
  default = "dw_admin"
}
variable "db_pswd" {}
variable "sku_name" {
  default = "B_Gen5_1"
  # Structure:
  # Basic (B) Memory Optimized (MO), General Purpose (GP)
  # Generation/Family
  # Cores
}
variable "db_version" {
  default = "10"
}
variable "db_server_storage_mb" {
  default = 5120
}

variable "db_server_capacity" {
  default = 1
}

variable "db_server_backup_retention_days" {
  default = 7
}

variable "db_server_geo_redundant_backup" {
  default = "Disabled"
}

variable "db_server_auto_grow" {
  default = "Disabled"
}

# Firewall
variable "start_ip_address" {
  default = "0.0.0.0"
}
variable "end_ip_address" {
  default = "0.0.0.0"
}