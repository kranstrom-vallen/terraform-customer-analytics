output "id" {
  value       = azurerm_analysis_services_server.main.id
  description = "Analysis Server Id"
}

output "server_full_name" {
  value       = azurerm_analysis_services_server.main.server_full_name
  description = "Analysis Server Full name"
}