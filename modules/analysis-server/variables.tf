variable "customer" {}
variable "environment" {}

# Shared
variable "resource_group_name" {}
variable "location" {}

variable "sku" {
  // D1, B1, B2, S0, S1, S2, S4, S8 and S9
  default = "B1"
}

variable "admin_users" {
  default = []
}

# variable "ip_start" {
#     default = ""
# }
# variable "ip_end" {
#     default = ""
# }