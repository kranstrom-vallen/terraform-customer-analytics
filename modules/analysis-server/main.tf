resource "azurerm_analysis_services_server" "main" {
  name                = "cid${var.customer}"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = var.sku
  #   admin_users             = var.admin_users
  enable_power_bi_service = true

  #   ipv4_firewall_rule {
  #     name        = "aass-${var.customer}-r1"
  #     range_start = var.ip_start
  #     range_end   = var.ip_end
  #   }

  tags = {
    Organization = "${var.customer}"
    Resource     = "analytics"
    Environment  = var.environment
  }
}