output "ss_id" {
  value       = azurerm_sql_server.dw.id
  description = "SQL Server Id"
}

output "fully_qualified_domain_name" {
  value       = azurerm_sql_server.dw.fully_qualified_domain_name
  description = "SQL Server FQDN"
}

output "db_id" {
  value       = azurerm_sql_database.dw.id
  description = "SQL Server Database Id"
}