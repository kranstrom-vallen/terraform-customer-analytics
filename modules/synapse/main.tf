resource "azurerm_sql_server" "dw" {
  name                         = "dw-${var.customer}"
  resource_group_name          = var.resource_group_name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = var.db_user
  administrator_login_password = var.db_pswd
  tags = {
    Organization = var.customer
    Resource     = "storage"
    Environment  = var.environment
  }
}

resource "azurerm_sql_firewall_rule" "dw" {
  name                = "dw-fwr-${var.customer}"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_sql_server.dw.name
  start_ip_address    = var.start_ip_address
  end_ip_address      = var.end_ip_address
}

resource "azurerm_sql_database" "dw" {
  name                = "dw-db-${var.customer}"
  resource_group_name = var.resource_group_name
  location            = var.location
  server_name         = azurerm_sql_server.dw.name
  create_mode         = "Default"
  edition             = var.db_edition
  # max_size_bytes                   = var.db_max_size_bytes
  requested_service_objective_name = var.db_service

  threat_detection_policy {
    state                      = "Enabled"
    email_account_admins       = "Enabled"
    email_addresses            = var.threat_detection_emails
    retention_days             = var.threat_detection_retention_days
    storage_account_access_key = var.threat_detection_storage_account_access_key
    storage_endpoint           = var.threat_detection_storage_endpoint
  }

  tags = {
    Organization = var.customer
    Resource     = "storage"
    Environment  = var.environment
  }
}