# Customer/Environment
variable "customer" {}
variable "environment" {}

# Shared
variable "resource_group_name" {}
variable "location" {}

# Firewall
variable "start_ip_address" {
  default = "0.0.0.0"
}
variable "end_ip_address" {
  default = "0.0.0.0"
}

# DB-specific
variable "db_user" {
  default = "dw_admin"
}
variable "db_pswd" {}
variable "db_edition" {
  default = "DataWarehouse"
}
variable "db_max_size_bytes" {
  # 100MB
  default = 100000000
}
variable "db_service" {
  default = "DW100c"
}

variable "threat_detection_emails" {
  default = []
}
variable "threat_detection_storage_account_access_key" {}
variable "threat_detection_storage_endpoint" {}
variable "threat_detection_retention_days" {
  default = 7
}

# variable tags {
#   type    = map(string)
#   default = {}
# }