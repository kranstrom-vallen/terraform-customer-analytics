resource "azurerm_resource_group" "main" {
  name     = "ResourceGroup${var.customer}"
  location = var.location

  tags = {
    Organization = var.customer
    Resource     = "resource-group"
    Environment  = var.environment
  }
}

resource "azurerm_storage_account" "main" {
  name                      = "saspvcid${var.customer}"
  resource_group_name       = azurerm_resource_group.main.name
  location                  = azurerm_resource_group.main.location
  account_kind              = "BlobStorage"
  account_tier              = "Standard"
  account_replication_type  = "GRS"
  access_tier               = "Hot"
  enable_https_traffic_only = true

  tags = {
    Organization = var.customer
    Resource     = "storage"
    Environment  = var.environment
  }
}

# module "dw_db" {
#   source = "../synapse"

#   resource_group_name = azurerm_resource_group.main.name
#   location            = azurerm_resource_group.main.location
#   customer            = var.customer
#   environment         = var.environment
#   db_user             = var.db_user
#   db_pswd             = var.db_pswd

#   threat_detection_emails                     = var.admin_emails
#   threat_detection_storage_account_access_key = azurerm_storage_account.main.primary_access_key
#   threat_detection_storage_endpoint           = azurerm_storage_account.main.primary_blob_endpoint
# }

module "dw_pg_db" {
  source = "../postgres_db"

  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  customer            = var.customer
  environment         = var.environment
  db_user             = var.db_user
  db_pswd             = var.db_pswd
}

# Examples of modules that can be applied:

# module "analysis_server" {
#   source = "../analysis-server"

#   resource_group_name = azurerm_resource_group.main.name
#   location            = azurerm_resource_group.main.location
#   customer            = var.customer
#   environment         = var.environment
# }

# module "powerbi" {
#   source = "../powerbi-embedded"

#   resource_group_name = azurerm_resource_group.main.name
#   location            = azurerm_resource_group.main.location
#   customer            = var.customer
#   environment         = var.environment
# }