variable "location" {
  default = "East US 2"
}

variable "customer" {}
variable "environment" {}
variable "admin_emails" {
  default = []
}

# DB Specific
variable "db_user" {
  default = "dw_admin"
}
variable "db_pswd" {}